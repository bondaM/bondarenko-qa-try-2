/*9. ������� �����/�������� ����������� � ����� �������� ���������� (TerritoryDescription), 
�� ������� ��� ��������, � ��������� ������� (RegionDescription). ��� ���������� ������ ������������ JOIN*/
SELECT FirstName, LastName, TerritoryDescription, RegionDescription
FROM 
((Employees INNER JOIN EmployeeTerritories 
			ON Employees.EmployeeID=EmployeeTerritories.EmployeeID)
INNER JOIN Territories ON Territories.TerritoryID=EmployeeTerritories.TerritoryID)
INNER JOIN Region ON Territories.RegionID=Region.RegionID
