/*10.	������� ���������� � �������� (������� Region), � �������� �� �������� �� ���� ��������� (Employee). 
��� ���������� ������ ������������ JOIN.*/
SELECT RegionDescription, COUNT(*) AS CountEmp
FROM 
((Employees INNER JOIN EmployeeTerritories 
			ON Employees.EmployeeID=EmployeeTerritories.EmployeeID)
INNER JOIN Territories ON Territories.TerritoryID=EmployeeTerritories.TerritoryID)
INNER JOIN Region ON Territories.RegionID=Region.RegionID
GROUP BY RegionDescription

/* �� ����������� �������, ������ �����, ��� ����� �������� ���*/
